package org.nrg.ccf.common.utilities.components;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
@Getter
@Lazy
public class EmailUtils {
	
	final List<String> enabledUserEmailList = new ArrayList<>();
	final List<String> disabledUserEmailList = new ArrayList<>();
	
	@Autowired
	public EmailUtils(QueryUtils queryUtils) {
		super();
		enabledUserEmailList.addAll(queryUtils.getEnabledUserEmailList());
		disabledUserEmailList.addAll(queryUtils.getDisabledUserEmailList());
	}
	
}
