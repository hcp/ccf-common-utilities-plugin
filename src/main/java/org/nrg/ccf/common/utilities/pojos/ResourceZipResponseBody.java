package org.nrg.ccf.common.utilities.pojos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResourceZipResponseBody implements StreamingResponseBody {
	
	/** NOTE:  Using File.getAbsolutePath() in this class because getCanonicalPath() resolves symbolic  *
	 *  links and the resulting files will be skipped in the download archive                          **/
	
	protected final ArrayList<File> _zipList = new ArrayList<>();
	private final byte[] _buffer = new byte[org.nrg.xft.utils.FileUtils.LARGE_DOWNLOAD];
	final String _catalogPath;
	final String _catalogDirPath;

	public ResourceZipResponseBody(XnatResourcecatalog preprocResource) {
		
		_catalogPath = preprocResource.getFullPath(CommonConstants.ROOT_PATH);
		_catalogDirPath = _catalogPath.replaceFirst("[^/]*/$", "");
		log.debug("catalogDirPath=" + _catalogDirPath);
		try {
			initializeZipList(preprocResource);
		} catch (IOException e) {
			log.error("ERROR:  Exception thrown populating zip file list",e);
		}
		
	}

	protected void initializeZipList(XnatResourcecatalog resource) throws IOException {
		final List<File> preprocFiles = ResourceUtils.getFiles(resource, CommonConstants.ROOT_PATH);
		for (final File f : preprocFiles) {
			_zipList.add(f);
		}
	}

	@Override
	public void writeTo(OutputStream output) throws IOException {
		try {
			final ZipOutputStream zip = new ZipOutputStream(output);
			for (final File file : _zipList) {
				final String filepath = file.getAbsolutePath(); 
				//log.debug("catalogDirPath=" + _catalogDirPath + " , filepath=" + filepath);
				final ZipEntry entry = new ZipEntry(filepath.replace(_catalogDirPath,""));
				entry.setTime(file.lastModified());
				zip.putNextEntry(entry);
				try (final InputStream input = new FileInputStream(file)) {
					int len;
					while ((len = input.read(_buffer)) > 0) {
						zip.write(_buffer, 0, len);
					}
					input.close();
				}
				zip.closeEntry();
			}
			zip.close();
			output.flush();
		} catch (Exception e) {
			log.debug("Exception thrown writing zip output stream: ", e);
			throw e;
		}
	}

}
