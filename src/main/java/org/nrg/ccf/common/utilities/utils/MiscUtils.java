package org.nrg.ccf.common.utilities.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MiscUtils {
	
	public static String arrayToString(final String[] arr) {
		return listToString(Arrays.asList(arr));
	}
		
	public static String listToString(List<?> list) {
		final Iterator<?> i = list.iterator();
		final StringBuilder sb = new StringBuilder();
		while (i.hasNext()) {
			sb.append(i.next().toString());
			if (i.hasNext()) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}
		
	public static String listToHtml(List<?> list) {
		final Iterator<?> i = list.iterator();
		final StringBuilder sb = new StringBuilder();
		while (i.hasNext()) {
			sb.append(i.next().toString());
			if (i.hasNext()) {
				sb.append("<br>");
				sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}

}
