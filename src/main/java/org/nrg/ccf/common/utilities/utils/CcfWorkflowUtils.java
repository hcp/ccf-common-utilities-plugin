package org.nrg.ccf.common.utilities.utils;

import org.nrg.action.ServerException;
import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.EventUtils.CATEGORY;
import org.nrg.xft.event.EventUtils.TYPE;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.utils.WorkflowUtils;

public class CcfWorkflowUtils {
	
	public static PersistentWorkflowI buildWorkflow(UserI user, XFTItem newItem, CATEGORY eventCategory, TYPE eventType, String workflowLabel, String workflowStatus) throws Exception {
		
		PersistentWorkflowI xwrk;
		try {
			xwrk = PersistentWorkflowUtils.buildOpenWorkflow(user, newItem, 
						EventUtils.newEventInstance(eventCategory , eventType, workflowLabel));
		} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
			throw new ServerException("ERROR:  Error creating workflow", e);
		}
		assert xwrk != null;
		return xwrk;
		
	}
	
	public static EventMetaI buildEvent(UserI user, XFTItem newItem, CATEGORY eventCategory, TYPE eventType, String workflowLabel, String workflowStatus) throws Exception {
		
		PersistentWorkflowI xwrk = buildWorkflow(user, newItem, eventCategory, eventType, workflowLabel, workflowStatus);
		assert xwrk != null;
		EventMetaI ci = xwrk.buildEvent();
		return ci;
		
	}
	
	public static void generateWorkflow(UserI user, XFTItem newItem, CATEGORY eventCategory, TYPE eventType, String workflowLabel, String workflowStatus) throws Exception {
		
		PersistentWorkflowI xwrk = buildWorkflow(user, newItem, eventCategory, eventType, workflowLabel, workflowStatus);
		assert xwrk != null;
		EventMetaI ci = xwrk.buildEvent();
		xwrk.setStatus(PersistentWorkflowUtils.COMPLETE);
		WorkflowUtils.save(xwrk, ci);		
		
	}
	
	public static void generateWorkflow(UserI user, XFTItem newItem, String workflowLabel, String workflowStatus) throws Exception {
		
		generateWorkflow(user, newItem, EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, workflowLabel, workflowStatus);
		
	}
	
	public static void generateStatusCompleteWorkflow(UserI user, XFTItem newItem, CATEGORY eventCategory, TYPE eventType, String workflowLabel) throws Exception {
		
		generateWorkflow(user, newItem, eventCategory, eventType, workflowLabel, PersistentWorkflowUtils.COMPLETE);
		
	}
	
	public static void generateStatusCompleteWorkflow(UserI user, XFTItem newItem, String workflowLabel) throws Exception {
		
		generateWorkflow(user, newItem, EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, workflowLabel, PersistentWorkflowUtils.COMPLETE);
		
	}

}
