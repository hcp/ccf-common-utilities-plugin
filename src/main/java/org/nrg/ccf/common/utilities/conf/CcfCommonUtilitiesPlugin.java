package org.nrg.ccf.common.utilities.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfCommonUtilitiesPlugin",
			name = "CCF Common Utilities Plugin",
			log4jPropertiesFile = "/META-INF/resources/ccfCommonLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.common.utilities.conf",
		"org.nrg.ccf.common.utilities.components"
	})
@Slf4j
public class CcfCommonUtilitiesPlugin {
	
	public CcfCommonUtilitiesPlugin() {
		log.info("Configuring the CCF Common Utilities Plugin.");
	}
	
}
