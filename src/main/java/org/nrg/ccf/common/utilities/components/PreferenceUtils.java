package org.nrg.ccf.common.utilities.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class PreferenceUtils {
	
	private JdbcTemplate _jdbcTemplate;
	
	private static final String REMOVE_SITE_LEVEL_PREFERENCE_QUERY = 
			"DELETE FROM xhbm_preference WHERE tool IN (SELECT id FROM xhbm_tool where tool_id=?) and scope=0;";
	
	@Autowired
	public PreferenceUtils(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
	}
	
	public int removeSiteLevelPreferencesForTool(final String toolId) {
		return _jdbcTemplate.update(REMOVE_SITE_LEVEL_PREFERENCE_QUERY,  toolId);
	}
	
}
