package org.nrg.ccf.common.utilities.utils;

import org.nrg.xdat.model.XnatImagescandataI;

public class ScanUtils {
	
	public static boolean isXa30Scan(XnatImagescandataI scan) {
		if (scan.getScanner_softwareversion() != null && scan.getScanner_softwareversion().toUpperCase().contains("XA30")) {
			return true;
		}
		return false;
	}

}
