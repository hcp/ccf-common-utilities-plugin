package org.nrg.ccf.common.utilities.constants;

public enum LockOperation {
	
	LOCK,
	UNLOCK

}
