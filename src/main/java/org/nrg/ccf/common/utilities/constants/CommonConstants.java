package org.nrg.ccf.common.utilities.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class CommonConstants {
	
	public static final String YYMMDD10_FORMAT = "yyyy-MM-dd";
	public static final String YYMMDD8_FORMAT = "yyyyMMdd";
	public static final String YYMMDDHHMMSS_FORMAT = "yyyy/MM/dd HH:mm:ss";
	public static final String TIME_FORMAT = "HH:mm:ss";
	public static final String ROOT_PATH = "/";
	public static final String CATXML_EXT = "_catalog.xml";
	public static final String LINKED_DATA_RESOURCE = "LINKED_DATA";
	public static final String NIFTI_RESOURCE = "NIFTI";
	public static final String DICOM_RESOURCE = "DICOM";
	public static final String DICOM_ORIG_RESOURCE = "DICOM_ORIG";
	public static final String FACEMASK_QC_RESOURCE = "DEFACE_QC";
	public static final String SNAPSHOTS_RESOURCE = "SNAPSHOTS";
	public static final String SECONDARY_RESOURCE = "secondary";
	public static final String RULES_OVERRIDE_REQUIRED_FIELD = "rules_override_required";
	public static final String OVERRIDE_AS_WARNING_FIELD = "treat_override_as_warning";
	public static enum ResourceType { SCAN, SESSION };
	
	public static DateFormat getDateFormat(final String fmt) {
		return new SimpleDateFormat(fmt);
	}
	
	public static DateFormat getFormatYYMMDD10() {
		return getDateFormat(YYMMDD10_FORMAT);
	}
	
	public static DateFormat getFormatYYMMDD8() {
		return getDateFormat(YYMMDD8_FORMAT);
	}
	
	public static DateFormat getFormatYYMMDDHHMMSS() {
		return getDateFormat(YYMMDDHHMMSS_FORMAT);
	}
	
	public static DateFormat getFormatTIME() {
		return getDateFormat(TIME_FORMAT);
	}
	
}
