package org.nrg.ccf.common.utilities.utils;

import java.util.List;

import org.nrg.action.ServerException;
import org.nrg.xdat.model.XnatSubjectdataI;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;
//import lombok.extern.slf4j.Slf4j;
import org.nrg.xft.utils.SaveItemHelper;

//@Slf4j
public class SubjectUtils {
	
	public static XnatSubjectdataI getSubject(String projId, String subjIdOrLabel, UserI user) {
		
		XnatSubjectdataI subj = XnatSubjectdata.getXnatSubjectdatasById(subjIdOrLabel, user, true);
		if (subj == null) { 

	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	    	
	    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
	        subcc1.addClause("xnat:subjectData/project", projId);
	        subcc1.addClause("xnat:subjectData/label", subjIdOrLabel);
	        cc.add(subcc1);
	    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
	    	subcc2.addClause("xnat:subjectData/sharing/share/project", projId);
	    	subcc2.addClause("xnat:subjectData/sharing/share/label", subjIdOrLabel);
	        cc.add(subcc2);

			final List<XnatSubjectdata> subjList = XnatSubjectdata.getXnatSubjectdatasByField(cc, user, true);
			if (!subjList.isEmpty()) {
				subj = subjList.get(0);
			}
		}
		return subj;
		
	}
	
	public static XnatSubjectdataI getSubjectByLabel(String projId, String subjLabel, UserI user) {
		
    	final CriteriaCollection cc=new CriteriaCollection("OR");
    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
        subcc1.addClause("xnat:subjectData/project", projId);
        subcc1.addClause("xnat:subjectData/label", subjLabel);
        cc.add(subcc1);
    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
    	subcc2.addClause("xnat:subjectData/sharing/share/project", projId);
    	subcc2.addClause("xnat:subjectData/sharing/share/label", subjLabel);
        cc.add(subcc2);

		final List<XnatSubjectdata> subjList = XnatSubjectdata.getXnatSubjectdatasByField(cc, user, true);
		return (!subjList.isEmpty()) ? subjList.get(0) : null;
		
	}
	
	public static XnatSubjectdataI getOrCreateSubjectByLabel(String projId, String subjLabel, UserI user) throws Exception {
		
    	final CriteriaCollection cc=new CriteriaCollection("OR");
    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
        subcc1.addClause("xnat:subjectData/project", projId);
        subcc1.addClause("xnat:subjectData/label", subjLabel);
        cc.add(subcc1);
    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
    	subcc2.addClause("xnat:subjectData/sharing/share/project", projId);
    	subcc2.addClause("xnat:subjectData/sharing/share/label", subjLabel);
        cc.add(subcc2);

		final List<XnatSubjectdata> subjList = XnatSubjectdata.getXnatSubjectdatasByField(cc, user, true);
		if (subjList.isEmpty()) {
			XnatSubjectdata subjectData = new XnatSubjectdata();
			subjectData.setProject(projId);
			subjectData.setLabel(subjLabel);
			subjectData.setId(XnatSubjectdata.CreateNewID());
			PersistentWorkflowI wrk;
			try {
				wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, subjectData.getItem(), 
							EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.AUTO_CREATE_SUBJECT));
			} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
				throw new ServerException("ERROR:  Error creating workflow", e);
			}
			EventMetaI ci = wrk.buildEvent();
			SaveItemHelper.authorizedSave(subjectData.getItem(), user, false, false, ci);
			return subjectData;
			
		} else {
			return subjList.get(0);
		}
		
	}
	
}
