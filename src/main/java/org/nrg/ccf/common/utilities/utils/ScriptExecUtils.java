package org.nrg.ccf.common.utilities.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.commons.utilities.services.CcfExecService;
import org.slf4j.Logger;

@Slf4j
public class ScriptExecUtils {
	
	public static ScriptResult execRuntimeCommand(final String cmd) {
		return execRuntimeCommand(new String[] { cmd }, new String[] {}, null, null);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, Logger logger) {
		return execRuntimeCommand(new String[] { cmd }, new String[] {}, null, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(new String[] { cmd }, new String[] {}, null, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final String[] envArray) {
		return execRuntimeCommand(new String[] { cmd }, envArray, null, null);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final String[] envArray, Logger logger) {
		return execRuntimeCommand(new String[] { cmd }, envArray, null, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final String[] envArray, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(new String[] { cmd }, envArray, null, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray) {
		return execRuntimeCommand(cmdArray, new String[] {}, null, null);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, Logger logger) {
		return execRuntimeCommand(cmdArray, new String[] {}, null, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(cmdArray, new String[] {}, null, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, final String[] envArray) {
		return execRuntimeCommand(cmdArray, envArray, null, null);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, final String[] envArray, Logger logger) {
		return execRuntimeCommand(cmdArray, envArray, null, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, final String[] envArray, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(cmdArray, envArray, null, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final Integer timeoutMinutes) {
		return execRuntimeCommand(new String[] { cmd }, timeoutMinutes);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final Integer timeoutMinutes, Logger logger) {
		return execRuntimeCommand(new String[] { cmd }, timeoutMinutes, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final Integer timeoutMinutes, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(new String[] { cmd }, timeoutMinutes, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final String[] envArray, final Integer timeoutMinutes) {
		return execRuntimeCommand(new String[] { cmd }, envArray, timeoutMinutes);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final String[] envArray, final Integer timeoutMinutes, Logger logger) {
		return execRuntimeCommand(new String[] { cmd }, envArray, timeoutMinutes, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String cmd, final String[] envArray, final Integer timeoutMinutes, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(new String[] { cmd }, envArray, timeoutMinutes, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray,final Integer timeoutMinutes) {
		return execRuntimeCommand(cmdArray, new String[] {}, timeoutMinutes);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray,final Integer timeoutMinutes, Logger logger) {
		return execRuntimeCommand(cmdArray, new String[] {}, timeoutMinutes, logger);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray,final Integer timeoutMinutes, Logger logger, boolean infoLogCommand) {
		return execRuntimeCommand(cmdArray, new String[] {}, timeoutMinutes, logger, infoLogCommand);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, final String[] envArray,final Integer timeoutMinutes) {
		return execRuntimeCommand(cmdArray, envArray, timeoutMinutes, null);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, final String[] envArray,final Integer timeoutMinutes, Logger logger) {
		return execRuntimeCommand(cmdArray, envArray, timeoutMinutes, null, false);
	}
	
	public static ScriptResult execRuntimeCommand(final String[] cmdArray, final String[] envArray,final Integer timeoutMinutes, Logger logger, boolean infoLogCommand) {
		Integer returnCode = null;
		boolean returnSuccess = false;
		String returnStatus = "UNKNOWN";
		final List<String> stdoutList = new ArrayList<String>();	
		final List<String> stderrList = new ArrayList<String>();	
        final Logger currentLogger = (logger != null) ? logger : log;
        final String cmdArrayStr = MiscUtils.arrayToString(cmdArray);
		try {
            if (infoLogCommand) {
            	currentLogger.info("Executing Command: {}", cmdArrayStr);
            } else {
            	currentLogger.debug("Executing Command: {}", cmdArrayStr);
            }
			final Process process = (cmdArray.length>1) ? Runtime.getRuntime().exec(cmdArray, envArray) : Runtime.getRuntime().exec(cmdArrayStr, envArray);
		    final LineStreamConsumer stdout = new LineStreamConsumer(process.getInputStream()),
		    		stderr = new LineStreamConsumer(process.getErrorStream());
		    try {
		    	Integer rc = null;
		        stdout.start();
		        stderr.start();
		        if (currentLogger.isDebugEnabled()) {
		        	final String notifyStr =  String.format("executing command as %s with stdout, stderr consumers %s %s",
		                    new Object[]{cmdArrayStr, stdout, stderr});
		            currentLogger.debug(notifyStr);
		            //returnList.add(notifyStr);
		        }
		        if (timeoutMinutes != null) {
		        	final ExecutorService executor = CcfExecService.getSingleUseExecutorService();
			        executor.submit(new Callable<Boolean>() {
			        	public Boolean call() {
			        		int totalTime = 0;
		        			currentLogger.trace("Starting process monitor: {}", cmdArrayStr);
		        			try {
		        				Integer exitValue = null;
				        		while (exitValue == null && (timeoutMinutes==null || totalTime<(timeoutMinutes*60*1000))) {
				        			try {
										Thread.sleep(10000);
										totalTime = totalTime + 10000;
									} catch (InterruptedException e) {
										return true;
									}
				        			// TODO:  Java 8 adds a process.isAlive() method which could be used to check running status without an exception.
				        			// Wrapping the rc object in a final field, didn't work.  The value was never updated from null, so we need
				        			// to check the final process object.
				        			try { 
				        				exitValue = process.exitValue();
				        			} catch (Exception e) {
				        				// Do nothing
				        			}
				        		}
				        		if (exitValue == null) {
				        			final String errMsg = "APPLICATION ERROR:  Process exceeded timeout limit (" + timeoutMinutes + 
				        					" minutes).  It is being destroyed.  (COMMAND=" + Arrays.asList(cmdArray).toString() + "}";
				        			currentLogger.error(errMsg);
				        			stderrList.add(errMsg);
				        			process.destroy();
				        			return false;
				        		} else {
			        				currentLogger.trace("MONITOR:  process has ended - {} - {}", totalTime, cmdArrayStr);
				        		}
				        		return true;
		        			} catch (Throwable t) {
		        				currentLogger.error(ExceptionUtils.getFullStackTrace(t));
				        		return true;
		        			}
			        	}
			        });
		        }
		        rc = process.waitFor();
   				currentLogger.trace("APPLICATION:  process has ended - {}", cmdArrayStr);
		        returnCode = rc;
		        stdoutList.addAll(stdout.getLines());
		        stderrList.addAll(stderr.getLines());
		        if (0 == rc) {
		        	returnSuccess = true;
		        	returnStatus = "OK";
		        } else {
		        	returnStatus = "ERROR";
		        }
		    } catch (InterruptedException e) {
		        	returnStatus = "ERROR";
		     		currentLogger.error(ExceptionUtils.getFullStackTrace(e));
			} finally {
		       stdout.close();
		       stderr.close();
		    }
		} catch (IOException e) {
     		currentLogger.error(ExceptionUtils.getFullStackTrace(e));
		}
		return new ScriptResult(cmdArrayStr,returnSuccess,returnStatus,returnCode,stdoutList,stderrList);
	}
	
}
