package org.nrg.ccf.common.utilities.components;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class QueryUtils {
	
	private JdbcTemplate _jdbcTemplate;
	
	private static final String ENABLED_USER_QUERY = "SELECT login AS username FROM xdat_user " + 
			"JOIN xdat_user_meta_data ON xdat_user.user_info=xdat_user_meta_data.meta_data_id " +
			" WHERE (xdat_user.enabled=1) ORDER BY username;";
	
	private static final String DISABLED_USER_QUERY = "SELECT login AS username FROM xdat_user " + 
			"JOIN xdat_user_meta_data ON xdat_user.user_info=xdat_user_meta_data.meta_data_id " +
			" WHERE (xdat_user.enabled<1) ORDER BY username;";
	
	private static final String ENABLED_USER_EMAIL_QUERY = "SELECT email AS emailaddr FROM xdat_user " + 
			"JOIN xdat_user_meta_data ON xdat_user.user_info=xdat_user_meta_data.meta_data_id " +
			" WHERE (xdat_user.enabled=1) ORDER BY emailaddr;";
	
	private static final String DISABLED_USER_EMAIL_QUERY = "SELECT email AS emailaddr FROM xdat_user " + 
			"JOIN xdat_user_meta_data ON xdat_user.user_info=xdat_user_meta_data.meta_data_id " +
			" WHERE (xdat_user.enabled<1) ORDER BY emailaddr;";

	@Autowired
	public QueryUtils(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
	}
	
	public List<String> getDisabledUserList() {
		return _jdbcTemplate.queryForList(DISABLED_USER_QUERY, String.class);
	}
	
	public List<String> getEnabledUserList() {
		return _jdbcTemplate.queryForList(ENABLED_USER_QUERY, String.class);
	}
	
	public List<String> getEnabledUserEmailList() {
		return _jdbcTemplate.queryForList(ENABLED_USER_EMAIL_QUERY, String.class);
	}
	
	public List<String> getDisabledUserEmailList() {
		return _jdbcTemplate.queryForList(DISABLED_USER_EMAIL_QUERY, String.class);
	}

}
