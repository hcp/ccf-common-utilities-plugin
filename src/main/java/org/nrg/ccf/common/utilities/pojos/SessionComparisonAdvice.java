package org.nrg.ccf.common.utilities.pojos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionComparisonAdvice {
	
	private Boolean allowAddedRemoteFiles = true;
	private Boolean allowDifferingScanTypes = true;
	private Boolean allowDifferingSeriesDescriptions = false;
	private Boolean allowRemoteSetUnusable = true;
	private Boolean allowRemoteSetUsable = false;
	private List<String> excludedSessionResources;
	private List<String> fileCountOnlySessionResources;
	private List<String> excludedScanResources;
	private List<String> fileCountOnlyScanResources;
	private Map<String,Map<String, String>> alternateFilePathTransforms;
	
	public SessionComparisonAdvice(List<String> excludedSessionResources, List<String> fileCountOnlySessionResources, 
			List<String> excludedScanResources, List<String> fileCountOnlyScanResources, 
			Map<String, Map<String, String>> alternateFilePathTransforms) {
		super();
		this.excludedSessionResources = excludedSessionResources;
		this.fileCountOnlySessionResources = fileCountOnlySessionResources;
		this.excludedScanResources = excludedScanResources; 
		this.fileCountOnlyScanResources = fileCountOnlyScanResources; 
		this.alternateFilePathTransforms = alternateFilePathTransforms; 
	}
	
	public SessionComparisonAdvice(List<String> excludedSessionResources, List<String> fileCountOnlySessionResources, 
			List<String> excludedScanResources, List<String> fileCountOnlyScanResources) {
		this(excludedSessionResources, fileCountOnlySessionResources, 
				excludedScanResources, fileCountOnlyScanResources, new HashMap<>()); 
	}
	
	public SessionComparisonAdvice() {
		this(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), 
				new ArrayList<>(), new HashMap<>());
	}
	
}
