package org.nrg.ccf.common.utilities.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CcfFileUtils {
	
	public static void pruneEmptySubdirectories(File dir) {
		
		if (!(dir.exists() && dir.isDirectory() && dir.canWrite())) {
			return;
		}
		while(true) {
			boolean foundEmpty = false;
			final Iterator<File> filtered = FileUtils.iterateFilesAndDirs(dir, FalseFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			while(filtered.hasNext()) {
				final File d = filtered.next(); 
				if (d.isDirectory() && d.list().length==0) {
					foundEmpty = true;
					try {
						FileUtils.deleteDirectory(d);
					} catch (IOException e) {
						log.error("Couldn't remove directory:  " + d.getAbsolutePath());
						break;
					}
				}
			}
			if (!foundEmpty) {
				break;
			}
		}
		
	}
	
	public static void removeBrokenSymlinks(File dir) {
		
		if (!(dir.exists() && dir.isDirectory() && dir.canWrite())) {
			return;
		}
		final Iterator<File> filtered = FileUtils.iterateFilesAndDirs(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		while(filtered.hasNext()) {
			final File f = filtered.next(); 
			final Path p = f.toPath();
			if (Files.isSymbolicLink(f.toPath())) {
				Path target;
				try {
					target = Files.readSymbolicLink(p);
					// When we have a relative path, we need to append it to the parent of the current file.  Otherwise
					// the link will show up as broken when we test file.exists.
					if (target.startsWith("..")) {
						target = Paths.get(p.getParent().toString(),target.toString()); 
						
					}
					File targetF = target.toFile();
					if (!targetF.exists()) {
						log.warn("REMOVING BROKEN LINK " + p.toString());
						f.delete();
					}
				} catch (IOException e) {
					log.error("Error thrown check for or removing broken symlink " + p.toString(), e);
				}
			}
		}
	}

}
