package org.nrg.ccf.common.utilities.components;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.common.utilities.utils.SubjectUtils;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.services.SerializerService;
import org.nrg.mail.services.MailService;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.model.XnatSubjectdataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XsyncXsyncassessordata;

import org.nrg.xft.XFTItem;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXReader;
import org.nrg.xft.schema.Wrappers.XMLWrapper.SAXWriter;
import org.nrg.xft.security.UserI;
//import org.nrg.xft.utils.FileUtils;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.turbine.utils.ArcSpecManager;
import org.nrg.xnat.utils.WorkflowUtils;
import org.nrg.xsync.configuration.ProjectSyncConfiguration;
import org.nrg.xsync.connection.RemoteConnectionManager;
import org.nrg.xsync.exception.XsyncNotConfiguredException;
import org.nrg.xsync.local.SingleExperimentTransfer;
import org.nrg.xsync.remote.alias.services.SyncStatusService;
import org.nrg.xsync.services.local.SyncManifestService;
import org.nrg.xsync.tools.XsyncXnatInfo;
import org.nrg.xsync.utils.QueryResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class XsyncUtils {
	
	private Map<String,ProjectSyncConfiguration> _cacheMap = new HashMap<>(); 
	private JdbcTemplate _jdbcTemplate;
	private NamedParameterJdbcTemplate _npJdbcTemplate;
	private ConfigService _configService;
	private SerializerService _serializer;
	private QueryResultUtil _queryResultUtil;
    private MailService _mailService;
    private CatalogService _catalogService;
    private XsyncXnatInfo _xnatInfo;
    private SyncStatusService _syncStatusService;
    @SuppressWarnings("unused")
	private SyncManifestService _syncManifestService;
    private ExecutorService _executorService;
    private RemoteConnectionManager _manager;
	
	@Autowired
	public XsyncUtils(ConfigService configService, NamedParameterJdbcTemplate npJdbcTemplate, SerializerService serializer, QueryResultUtil queryResultUtil,
			MailService mailService, CatalogService catalogService, XsyncXnatInfo xnatInfo, 
			SyncStatusService syncStatusService, SyncManifestService syncManifestService, ThreadPoolExecutorFactoryBean executorFactoryBean,
			RemoteConnectionManager manager, JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
		_npJdbcTemplate = npJdbcTemplate;
		_configService = configService;
		_serializer = serializer;
		_queryResultUtil = queryResultUtil;
		_mailService = mailService;
		_catalogService = catalogService;
		_xnatInfo = xnatInfo;
		_syncStatusService = syncStatusService;
		_syncManifestService = syncManifestService;
        _manager = manager;
        _executorService = executorFactoryBean.getObject();
	}
	
	public ProjectSyncConfiguration getProjectSyncConfiguration(String projectId, UserI user) {
		if (!_cacheMap.containsKey(projectId)) {
			try {
				final ProjectSyncConfiguration newConfig = new ProjectSyncConfiguration(_configService, _serializer, _jdbcTemplate, projectId, user);
				if (newConfig != null) {
					_cacheMap.put(projectId, newConfig);
				}
			} catch (XsyncNotConfiguredException e) {
				log.warn("ERROR:  XSync not configured for project (" + projectId + ")", e);
			}
		}
		return _cacheMap.get(projectId);
	}
	
	
	// This is a lightly modified version of XsyncOperationsController.syncSingleSExperiment, but that version couldn't be called directly
    public ResponseEntity<String> syncSingleExperiment(final String experimentId, final UserI user, boolean waitForIt) throws URISyntaxException, XsyncNotConfiguredException {
        
    	//Is the experiment ID in the DB
    	XnatExperimentdata exp = XnatExperimentdata.getXnatExperimentdatasById(experimentId, user, false);
    	if (exp == null) {
    		//Incorrect Experiment ID
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    	//Get the project id
    	String projectId = exp.getProject(); 
    	//Check user credentials to see if the user is a member or an owner of the project
    	try {
        	final HttpStatus status = canDeleteProject(projectId, user);
            if (status != null) {
                return new ResponseEntity<>(status);
            }
        }catch(Exception e) {
            if (log.isInfoEnabled()) {
                log.info("Unable to fetch user permissions for user " + user.getLogin()  + " Project " + projectId );
            }
        }
    	if (_syncStatusService.isCurrentlySyncing(projectId)) {
    		return new ResponseEntity<>(HttpStatus.LOCKED);
    	}
    	
        final List<XsyncXsyncassessordata> okToSyncDatas = XsyncXsyncassessordata.getXsyncXsyncassessordatasByField("xsync:xsyncAssessorData/synced_experiment_id", experimentId, user, true);
        final XsyncXsyncassessordata okToSyncData;
    	if (okToSyncDatas != null && okToSyncDatas.size() > 0) {
            okToSyncData = okToSyncDatas.get(0);
            if (!okToSyncData.getSyncStatus().equals(org.nrg.xsync.utils.XsyncUtils.SYNC_STATUS_WAITING_TO_SYNC)) {
            	okToSyncData.setSyncStatus(org.nrg.xsync.utils.XsyncUtils.SYNC_STATUS_WAITING_TO_SYNC);
            	//Backward compatible XNAT 1.6.5 does not have ADMIN_EVENT method
            	final EventMetaI c = EventUtils.DEFAULT_EVENT(user, "ADMIN_EVENT occurred");
            	boolean saved;
            	try {
            		saved = okToSyncData.save(user, false, true, c);
            		if (!saved) {
            			return new ResponseEntity<>("Unable to mark sync assessor for syncing.", HttpStatus.INTERNAL_SERVER_ERROR);
            		}
            	} catch (Exception e) {
            		log.error("Unable to mark sync assessor for syncing:  " + ExceptionUtils.getFullStackTrace(e));
            		return new ResponseEntity<>("Unable to mark sync assessor for syncing.", HttpStatus.INTERNAL_SERVER_ERROR);
            	}
           	}
        } 
    	
    	final SingleExperimentTransfer singleExperimentTransfer = new SingleExperimentTransfer(_manager, _configService, _serializer,
    			_queryResultUtil, _npJdbcTemplate, _mailService,_catalogService, _xnatInfo, _syncStatusService, projectId, user, exp.getId());
		//_executorService.submit(singleExperimentTransfer);
        Future<Void> future = _executorService.submit(singleExperimentTransfer);
        if (waitForIt) {
	       	try {
				future.get(8, TimeUnit.HOURS);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				return new ResponseEntity<>("ERROR:  " + experimentId + " synchronization failed - " + e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
			}
        }
        if (log.isInfoEnabled()) {
            log.info("Experiment[ " + exp.getLabel() + "@" + projectId +"]" + experimentId + " is being exported by " + user.getUsername());
        }
        return new ResponseEntity<>(experimentId + " synchronization started", HttpStatus.OK);
    }
   
	// This is a lightly modified version of XsyncOperationsController.syncSingleSExperiment, but that version couldn't be called directly
    public ResponseEntity<String> localSyncSingleExperiment(final String experimentId, final UserI user, String remoteProject) throws Exception {
        
    	//Is the experiment ID in the DB
    	final XnatExperimentdata exp = XnatExperimentdata.getXnatExperimentdatasById(experimentId, user, true);
    	if (exp == null) {
    		//Incorrect Experiment ID
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	}
    	//Get the project id
    	String projectId = exp.getProject(); 
    	//Check user credentials to see if the user is a member or an owner of the project
    	try {
        	final HttpStatus status = canDeleteProject(projectId, user);
            if (status != null) {
                return new ResponseEntity<>(status);
            }
        }catch(Exception e) {
            if (log.isInfoEnabled()) {
                log.info("Unable to fetch user permissions for user " + user.getLogin()  + " Project " + projectId );
            }
        }
    	
    	if (!(exp instanceof XnatImagesessiondata)) {
    		return new ResponseEntity<>("ERROR:  This functionality currently only works for Image Sessions", HttpStatus.NOT_IMPLEMENTED);
    	}
    	
    	
    	final XnatImagesessiondata currSess = (XnatImagesessiondata) exp;
    	
    	final String localProject = exp.getProject();
    	final String localLabel = exp.getLabel();
    	final String localSubject = currSess.getSubjectData().getLabel();
    	
		final File localArchive = new File(ArcSpecManager.GetInstance().getArchivePathForProject(localProject) + exp.getProjectData().getCurrentArc()); 
		final File localSessionArchive = new File(localArchive, localLabel);
    	
    	final XnatSubjectdataI remoteSubject = SubjectUtils.getOrCreateSubjectByLabel(remoteProject, localSubject, user);
    	final XnatExperimentdataI remoteExp = ExperimentUtils.getExperimentByLabel(remoteProject, localLabel, user);
    	final String newId;
    	if (remoteExp != null) {
    		newId = remoteExp.getId();
    	} else {
    		newId = XnatExperimentdata.CreateNewID();
    	}
    	currSess.setId(newId);
    	currSess.setSubjectId(remoteSubject.getId());
    	currSess.setProject(remoteProject);    	
		final File remoteArchive = new File(ArcSpecManager.GetInstance().getArchivePathForProject(remoteProject) + 
				XnatProjectdata.getXnatProjectdatasById(remoteProject, user, false).getCurrentArc()); 
		final File remoteSessionArchive = new File(remoteArchive, localLabel);
    	//for (final XnatImagescandataI scan : currSess.getScans_scan()) {
    	//	scan.setId(newId);
    	//}
    	final StringWriter sw = new StringWriter();
		SAXWriter writer = null;
		// I don't know why, but an exception is occasionally thrown getting the SAXWriter.  It's usually successful
		// on reruns of the same session.  Just putting it in a loop to try a few times before giving up.
		for (int i=0; i<5; i++) {
			try {
				writer = new SAXWriter(sw,false);
				break;
			} catch (Exception e) {
				log.debug("NOTE:  Exception thrown obtaining SAXWriter:  ", e);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException intE) {
					// Do nothing
				}
			}
		}
		if (writer == null) {
    		return new ResponseEntity<>("ERROR:  Couldn't get SAX Writer",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		writer.setWriteHiddenFields(false);
		writer.write((XFTItem)currSess.getItem());
		final SAXReader reader = new SAXReader(user);
		final String sessionXml = sw.toString().replaceAll(File.separator +  localProject + File.separator,File.separator +  remoteProject + File.separator);
		sw.close();
		final XFTItem newItem = reader.parse(new ByteArrayInputStream(sessionXml.getBytes()));
		//final XnatMrsessiondata newSession = new XnatMrsessiondata(newItem);
		PersistentWorkflowI wrk;
		PersistentWorkflowI xwrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, newItem, 
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.STORE_XML));
			xwrk = PersistentWorkflowUtils.buildOpenWorkflow(user, newItem, 
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, "Xsync (Local)"));
		} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
			throw new ServerException("ERROR:  Error creating workflow", e);
		}
		assert wrk != null;
		assert xwrk != null;
		EventMetaI ci = wrk.buildEvent();
		boolean rc = SaveItemHelper.authorizedSave(newItem, user, false, true, ci);
		if (rc) {
			// Now copy files
			if (localSessionArchive.exists() && localSessionArchive.isDirectory()) {
				if (remoteSessionArchive.exists() && remoteSessionArchive.isDirectory()) {
					FileUtils.deleteDirectory(remoteSessionArchive);
				}
				FileUtils.copyDirectory(localSessionArchive, remoteSessionArchive, true);
				//if (remoteSessionArchive.exists() && remoteSessionArchive.isDirectory()) {
				//	FileUtils.deleteDirQuietly(remoteSessionArchive);
				//}
				//FileUtils.CopyDir(localSessionArchive, remoteSessionArchive, false);
			}
			wrk.setStatus(PersistentWorkflowUtils.COMPLETE);
			xwrk.setStatus(PersistentWorkflowUtils.COMPLETE);
		} else {
			wrk.setStatus(PersistentWorkflowUtils.FAILED);
			xwrk.setStatus(PersistentWorkflowUtils.FAILED);
		}
		WorkflowUtils.save(wrk, ci);
		WorkflowUtils.save(xwrk, ci);
        return new ResponseEntity<>(experimentId + " synchronization completed", HttpStatus.OK);
    }
    
    protected HttpStatus canDeleteProject(final String projectId, UserI user) throws Exception {
        final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
        if (project == null) {
            return HttpStatus.NOT_FOUND;
        }
        if (!project.canDelete(user)) {
            return HttpStatus.FORBIDDEN;
        }
        return null;
    }
	
}
