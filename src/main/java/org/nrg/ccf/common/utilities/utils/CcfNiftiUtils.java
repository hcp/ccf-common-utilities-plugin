package org.nrg.ccf.common.utilities.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.nifti.NiftiFileInfo;
import org.nrg.ccf.common.utilities.nifti.XnatNiftiReader;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xnat.nifti.NiftiHeader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CcfNiftiUtils {
	
	public static NiftiFileInfo getNiftiFileInfo(final String absolutePath) {
			try {
				final XnatNiftiReader niftiReader = new XnatNiftiReader(absolutePath);
				return niftiReader.getNiftiFileInfo();
			} catch (Exception e) {
				// NOTE: The XnatNiftiReader seems to throw NullPointerExceptions for invalid NIFTI files
				log.info("ERROR:  Exception thrown reading NIFTI file (file=" + absolutePath + ")", e);
				return null;
			}
	}
	
	public static NiftiHeader getNiftiHeader(final String absolutePath) {
			try {
				final XnatNiftiReader niftiReader = new XnatNiftiReader(absolutePath);
				return niftiReader.getNiftiFileInfo().getNiftiHeader();
			} catch (Exception e) {
				// NOTE: The XnatNiftiReader seems to throw NullPointerExceptions for invalid NIFTI files
				log.info("ERROR:  Exception thrown reading NIFTI file (file=" + absolutePath + ")", e);
				return null;
			}
	}
	
	public static List<Integer> getNiftiDimensionSums(XnatResourcecatalog catalog) throws IOException {
		final Integer[] dimSums = new Integer[] { 0, 0, 0, 0 };
		@SuppressWarnings("unchecked")
		final List<File> files = catalog.getCorrespondingFiles(CommonConstants.ROOT_PATH);
		for (final File file : files) {
			final String entryName = file.getName();
			if (entryName == null) {
				continue;
			}
			final String lcEntryName = entryName.toLowerCase();
			if (!lcEntryName.contains(".nii")) {
				continue;
			}
			final String niftiPath = file.getCanonicalPath();
			final NiftiHeader niftiHeader = getNiftiHeader(niftiPath);
			if (niftiHeader == null) {
				log.error("ERROR:  Could not retrieve NIFTI header from file (file=" + niftiPath + ")");
				continue;
			}
			short[] niftiDims = niftiHeader.dim;
			// NOTE the shift here.  We don't want dim0.  We want dim1-dim4.
			for ( int i=0; i<=3; i++) {
				if (niftiDims.length>=(i+2)) {
					dimSums[i]+=niftiDims[i+1];
				}
			}
		}
		List<Integer> dimList = new ArrayList<Integer>();
		dimList.addAll(Arrays.asList(dimSums));
		// Add other possible values seen (for XA30 scans)
		try {
			dimList.add((dimList.get(2)*dimList.get(3)));
			dimList.add((dimList.get(2)/2)*dimList.get(3));
		} catch (Exception e) {
			log.error(e.toString());
		}
		return dimList;
	}
}
