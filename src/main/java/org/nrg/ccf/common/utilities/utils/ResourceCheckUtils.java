package org.nrg.ccf.common.utilities.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.xdat.model.CatEntryI;
import org.nrg.xdat.om.XnatResourcecatalog;

public class ResourceCheckUtils {
	
	public static boolean checkNiftiFiles(final XnatResourcecatalog catalog, final StringBuilder infoSB,
			final String scanId, final String seriesDesc) {
		if (catalog == null || catalog.getCatalog(CommonConstants.ROOT_PATH) == null) {
			return false;
		}
		int niiCount = 0;
		int jsonCount = 0;
		final List<CatEntryI> entries = catalog.getCatalog(CommonConstants.ROOT_PATH).getEntries_entry();
		for (final CatEntryI entry : entries) {
			final String entryName = entry.getName();
			if (entryName == null) {
				continue;
			}
			final String lcEntryName = entryName.toLowerCase();
			if (lcEntryName.contains(".nii")) {
				niiCount++;
			} else if (lcEntryName.contains(".json")) {
				jsonCount++;
			} 
		}
		if (niiCount<1) {
			infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - NIFTI resource contains no NIFTI files.</li>");
		}
		if (jsonCount<1) {
			infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - NIFTI resource contains no JSON files.</li>");
		}
		return (niiCount>=1 && jsonCount>=1);
	}

	public static boolean checkSnapshotFiles(XnatResourcecatalog catalog, StringBuilder infoSB, String scanId,
			String seriesDesc) {
		if (catalog == null || catalog.getCatalog(CommonConstants.ROOT_PATH) == null) {
			return false;
		}
		int gifCount = 0;
		final List<CatEntryI> entries = catalog.getCatalog(CommonConstants.ROOT_PATH).getEntries_entry();
		for (final CatEntryI entry : entries) {
			final String entryName = entry.getName();
			if (entryName != null  && entryName.toLowerCase().contains(".gif")) {
				gifCount++;
			}
		}
		if (gifCount<1) {
			infoSB.append("<li>SCAN " + scanId + " (" + seriesDesc + ") - SNAPSHOTS resource contains no GIF files.</li>");
		}
		return (gifCount>=1);
	}
	
	
	public static boolean hasInitialFramesFile(XnatResourcecatalog catalog) {
		if (catalog == null || catalog.getCatalog(CommonConstants.ROOT_PATH) == null) {
			return false;
		}
		final List<CatEntryI> entries = catalog.getCatalog(CommonConstants.ROOT_PATH).getEntries_entry();
		for (final CatEntryI entry : entries) {
			final String entryName = entry.getName();
			if (entryName == null) {
				continue;
			}
			final String lcEntryName = entryName.toLowerCase();
			if (lcEntryName.contains(".nii") && lcEntryName.contains("initialframes")) {
				return true;
			} 
		}
		return false;
	}
	
	
	public static boolean hasValidBidsJsonFile(XnatResourcecatalog catalog) {
		if (catalog == null || catalog.getCatalog(CommonConstants.ROOT_PATH) == null) {
			return false;
		}
		final List<CatEntryI> entries = catalog.getCatalog(CommonConstants.ROOT_PATH).getEntries_entry();
		int niftiCount=0;
		int jsonCount=0;
		for (final CatEntryI entry : entries) {
			final String entryName = entry.getName();
			if (entryName == null) {
				continue;
			}
			final String lcEntryName = entryName.toLowerCase();
			if (lcEntryName.contains(".nii") && !lcEntryName.contains("initialframes")) {
				niftiCount+=1;
			}
			if (lcEntryName.contains(".json")) {
				final File catalogFile = catalog.getCatalogFile(CommonConstants.ROOT_PATH);
				if (catalogFile == null || !catalogFile.exists()) {
					continue;
				}
				final String entryUri = entry.getUri();
				final File entryFile = new File(catalogFile.getParent() + 
						((entryUri.startsWith(File.separator)) ? entryUri : File.separator + entryUri));
				if (entryFile == null || !entryFile.exists()) {
					continue;
				}
				String entryContent;
				try {
					entryContent = FileUtils.readFileToString(entryFile);
				} catch (IOException e) {
					continue;
				}
				if (entryContent.length()<1) {
					return false;
				}
		        try {
		            new JSONObject(entryContent);
		            jsonCount+=1;
		        } catch (JSONException ex) {
		            try {
		                new JSONArray(entryContent);
		            } catch (JSONException ex1) {
		                return false;
		            }
		        }
			} 
		}
		return (jsonCount>=niftiCount);
	}

}
