package org.nrg.ccf.common.utilities.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
public class PojoUtils {
	
	public static Map<String, Object> pojoToMap(Object pojo) {
		
		final ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(pojo,  new TypeReference<LinkedHashMap<String, Object>>() {}); 
		
	}
	
	public static Map<String, String> pojoToStrMap(Object pojo) {
		
		final ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(pojo,  new TypeReference<LinkedHashMap<String, String>>() {}); 
	}
	
	public static List<String> pojoListToCsv(List<?> pojoList) {
		
		final List<String> returnList = new ArrayList<>();
        final Set<String> keys = new LinkedHashSet<>(pojoToStrMap(pojoList.get(0)).keySet());
        returnList.add(StringUtils.join(keys, ","));
        for (final Object pojo: pojoList) {
        	final StringBuilder sb = new StringBuilder();
        	final Map<String, String> objMap = pojoToStrMap(pojo);
        	final Iterator<String> i = keys.iterator(); 
        	while (i.hasNext()) {
        		final String key = i.next();
        		String val = objMap.get(key);
        		val = (val != null) ? val : "";
        		val = (!val.contains(",")) ? val : "\"" + val.replaceAll("\"", "\\\\\"") + "\"";
        		sb.append(val);
        		if (i.hasNext()) {
        			sb.append(",");
        		}
        		
        	}
        	returnList.add(sb.toString());
        }
		return returnList;
		
	}
	
	public static String pojoListToCsvStr(List<?> pojoList) {
		
		final StringBuilder sb = new StringBuilder();
		for (final String row : pojoListToCsv(pojoList)) {
			sb.append(row);
			sb.append(System.lineSeparator());
		}
		return sb.toString();
		
	}

}
