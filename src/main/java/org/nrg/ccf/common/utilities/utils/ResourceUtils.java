package org.nrg.ccf.common.utilities.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.constants.LockOperation;
import org.nrg.framework.exceptions.NrgServiceException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.model.XnatProjectdataI;
import org.nrg.xdat.model.XnatSubjectdataI;
import org.nrg.xdat.om.CatCatalog;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoXnatExperimentdata;
import org.nrg.xdat.om.base.auto.AutoXnatImagescandata;
import org.nrg.xdat.om.base.auto.AutoXnatImagesessiondata;
import org.nrg.xdat.om.base.auto.AutoXnatProjectdata;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.ActionNameAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.IDAbsent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.JustificationAbsent;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResourceUtils {

	public static XnatResourcecatalog getResource(final XnatProjectdataI proj, final String label)  {
    	if (proj == null) return null;
    	for (final XnatAbstractresourceI resource : proj.getResources_resource()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return (XnatResourcecatalog)resource;
    	}
    	return null;
	}

	public static XnatResourcecatalog getResource(final XnatSubjectdataI subj, final String label)  {
    	if (subj == null || label == null) return null;
    	for (final XnatAbstractresourceI resource : subj.getResources_resource()) {
    		if (resource == null || !(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return (XnatResourcecatalog)resource;
    	}
    	return null;
	}

	public static XnatResourcecatalog getResource(final XnatExperimentdataI exp, final String label)  {
    	if (exp == null) return null;
    	for (final XnatAbstractresourceI resource : exp.getResources_resource()) {
    		if (resource == null || !(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return (XnatResourcecatalog)resource;
    	}
    	return null;
	}

	public static XnatResourcecatalog getOrCreateResource(final XnatProjectdataI proj, final String label, final UserI user) throws ServerException, ClientException  {
		XnatResourcecatalog resource;
		if (!hasResource(proj, label)) {
			if (!(proj instanceof AutoXnatProjectdata)) {
				throw new ClientException("Project data cannot be converted to required type");
			}
			resource = ResourceUtils.createProjectResource((AutoXnatProjectdata) proj, label, user);
		} else {
			resource = ResourceUtils.getResource(proj, label);
		}
		return resource;
	}

	/*
	public static XnatResourcecatalog getOrCreateResource(final XnatSubjectdataI subj, final String label, final UserI user)  {
		// Not yet implemented
	}
	*/

	public static XnatResourcecatalog getOrCreateResource(final XnatExperimentdataI exp, final String label, final UserI user) throws ServerException, ClientException  {
		XnatResourcecatalog resource;
		if (!hasResource(exp, label)) {
			if (!(exp instanceof AutoXnatExperimentdata)) {
				throw new ClientException("Experiment data cannot be converted to required type");
			}
			resource = ResourceUtils.createSessionResource((AutoXnatExperimentdata) exp, label, user);
		} else {
			resource = ResourceUtils.getResource(exp, label);
		}
		return resource;
	}

	public static boolean hasResource(final XnatProjectdataI proj, final String label)  {
    	if (proj == null) return false;
    	for (final XnatAbstractresourceI resource : proj.getResources_resource()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return true;
    	}
    	return false;
	}

	public static boolean hasResource(final XnatSubjectdataI subj, final String label)  {
    	if (subj == null) return false;
    	for (final XnatAbstractresourceI resource : subj.getResources_resource()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return true;
    	}
    	return false;
	}

	public static boolean hasResource(final XnatExperimentdataI exp, final String label)  {
    	if (exp == null) return false;
    	for (final XnatAbstractresourceI resource : exp.getResources_resource()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return true;
    	}
    	return false;
	}

	public static XnatResourcecatalog getSingleResourceMatchingRegex(final XnatExperimentdataI exp, final String regex)  {
		final List<XnatResourcecatalog> returnList = getResourcesMatchingRegex(exp, regex);
    	if (returnList.size()==1) {
    		return returnList.get(0); 
    	} else if (returnList.size()>1) {
    		log.error("ERROR:  getSingleResourceMatchingRegex returned multple files.  Only one expected (REGEX=" +  regex + ")");
    		return null;
    	}
    	return null;
	}

	public static List<XnatResourcecatalog> getResourcesMatchingRegex(final XnatExperimentdataI exp, final String regex)  {
		final List<XnatResourcecatalog> returnList = new ArrayList<>();
    	if (exp == null) return null;
    	for (final XnatAbstractresourceI resource : exp.getResources_resource()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().matches(regex)) {
    			continue;
    		}
    		returnList.add((XnatResourcecatalog)resource);
    	}
    	return returnList;
	}
	
	// Archive path for resource
	public static File getResourcePathFile(XnatAbstractresourceI resource) {
		return (resource != null) ? new File(getResourcePath(resource)) : null;
	}
	
	// Archive path for resource
	public static String getResourcePath(XnatAbstractresourceI resource) {
		if (!(resource instanceof XnatResourcecatalog)) {
			return null;
		}
		XnatResourcecatalog resourceCatalog = (XnatResourcecatalog)resource;
		return resourceCatalog.getCatalogFile(CommonConstants.ROOT_PATH).getParent();
	}
	
	public static List<File> getFiles(XnatAbstractresourceI resource) {
		// Let's default to ROOT_PATH because in practice we don't have any other PATH in Intradb
		return getFiles(resource, CommonConstants.ROOT_PATH);
	}
	
	public static List<File> getFiles(XnatAbstractresourceI resource, String rootPath) {
		// NOTE:  resource.getCorrespondingFiles() with no ROOT_PATH specified sometimes returns nulls.
		final List<File> returnList = new ArrayList<>();
		if (resource == null || !(resource instanceof XnatResourcecatalog)) {
			return returnList;
		}
		XnatResourcecatalog resourceCatalog = (XnatResourcecatalog)resource;
		List<?> fileList = (resourceCatalog).getCorrespondingFiles(rootPath);
		if (fileList == null) {
			fileList = resourceCatalog.getCorrespondingFiles();
			if (fileList == null) {
				log.warn("WARNING:  fileList returned null for (resource=" + resourceCatalog.getUri() + ")");
				return returnList;
			}
		}
		for (Object fObj : fileList) {
			if (fObj instanceof File) {
				returnList.add((File)fObj);
			}
		}
		return returnList;
	}
	
	public static File getFileByFileName(XnatAbstractresourceI resource, String fileName) {
		for (Object fObj : getFiles(resource)) {
			if (!(fObj instanceof File)) {
				continue;
			}
			File f = (File)fObj;
			if (f.getName().equals(fileName)) {
				return f;
			}
		}
		return null;
	}
	
	public static List<File> getFilesByFileNameRegex(XnatAbstractresourceI resource, String regex) {
		final List<File> returnList = new ArrayList<>();
		for (Object fObj : getFiles(resource)) {
			if (!(fObj instanceof File)) {
				continue;
			}
			File f = (File)fObj;
			if (f.getName().matches(regex)) {
				returnList.add(f);
			}
		}
		return returnList;
	}

	public static File getSingleFileMatchingRegex(final XnatAbstractresourceI resource, final String regex)  {
		final List<File> returnList = getFilesByFileNameRegex(resource, regex);
    	if (returnList.size()==1) {
    		return returnList.get(0); 
    	} else if (returnList.size()>1) {
    		log.error("ERROR:  getSingleResourceMatchingRegex returned multple files.  Only one expected (REGEX=" +  regex + ")");
    		return null;
    	}
    	return null;
	}
	
	public static List<File> getFilesByFilePathRegex(XnatAbstractresourceI resource, String regex) {
		final List<File> returnList = new ArrayList<>();
		for (Object fObj : getFiles(resource)) {
			if (!(fObj instanceof File)) {
				continue;
			}
			File f = (File)fObj;
			if (f.getPath().matches(regex)) {
				returnList.add(f);
			}
		}
		return returnList;
	}
	
	public static List<String> getFileNames(XnatResourcecatalog resource) {
		// Let's default to ROOT_PATH because in practice we don't have any other PATH in Intradb
		return getFileNames(resource, CommonConstants.ROOT_PATH);
	}
	
	public static List<String> getFileNames(XnatResourcecatalog resource, String rootPath) {
		// NOTE:  resource.getCorrespondingFiles() with no ROOT_PATH specified sometimes returns nulls.
		final List<String> returnList = new ArrayList<>();
		List<?> fileList = resource.getCorrespondingFileNames(rootPath);
		if (fileList == null) {
			@SuppressWarnings("unchecked")
			List<File> files = resource.getCorrespondingFiles(rootPath);
			if (files == null) {
				log.warn("WARNING:  fileList returned null for (resource=" + resource.getUri() + ")");
				return returnList;
			}
			for (final File file : files) {
				returnList.add(file.getName());
			}
		} else {
			for (final Object obj : fileList) {
				returnList.add(obj.toString());
			}
		}
		return returnList;
	}
	
	public static List<String> getRelativeFilePaths(XnatResourcecatalog resource) {
		// Let's default to ROOT_PATH because in practice we don't have any other PATH in Intradb
		return getRelativeFilePaths(resource, CommonConstants.ROOT_PATH);
	}
	
	public static List<String> getRelativeFilePaths(XnatResourcecatalog resource, String rootPath) {
		// NOTE:  resource.getCorrespondingFiles() with no ROOT_PATH specified sometimes returns nulls.
		final List<String> returnList = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<File> files = resource.getCorrespondingFiles(rootPath);
		if (files == null) {
			log.warn("WARNING:  fileList returned null for (resource=" + resource.getUri() + ")");
			return returnList;
		}
		for (final File file : files) {
			returnList.add(file.getPath().replaceFirst(getResourcePath(resource) + "/", ""));
		}
		return returnList;
	}
	
	public static List<String> getFilesystemFilePaths(XnatResourcecatalog resource) {
		// Let's default to ROOT_PATH because in practice we don't have any other PATH in Intradb
		return getFilesystemFilePaths(resource, CommonConstants.ROOT_PATH);
	}
	
	public static List<String> getFilesystemFilePaths(XnatResourcecatalog resource, String rootPath) {
		// NOTE:  resource.getCorrespondingFiles() with no ROOT_PATH specified sometimes returns nulls.
		final List<String> returnList = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<File> files = resource.getCorrespondingFiles(rootPath);
		if (files == null) {
			log.warn("WARNING:  fileList returned null for (resource=" + resource.getUri() + ")");
			return returnList;
		}
		for (final File file : files) {
			returnList.add(file.getPath());
		}
		return returnList;
	}
	
	public static Long getRecentFileModTime(XnatResourcecatalog resource) {
		return getRecentFileModTime(resource, null);
	}
	
	public static Long getRecentFileModTime(XnatResourcecatalog resource, Integer maxCheckedFiles) {
		if (resource == null) {
			return null;
		}
		int counter = ( maxCheckedFiles != null ) ? maxCheckedFiles : Integer.MAX_VALUE; 
		final List<File> files = getFiles(resource, CommonConstants.ROOT_PATH);
		Long returnDate = null;
		for (final File f : files) {
			counter = counter - 1;
			long modTime = f.lastModified();
			returnDate = (returnDate == null || returnDate>modTime) ? modTime : returnDate;
			if (counter <= 0) {
				break;
			}
		}
		return returnDate;
	}
	
	public static Long getEarliestFileModTime(XnatResourcecatalog resource) {
		return getEarliestFileModTime(resource, null);
	}
	
	public static Long getEarliestFileModTime(XnatResourcecatalog resource, Integer maxCheckedFiles) {
		if (resource == null) {
			return null;
		}
		int counter = ( maxCheckedFiles != null ) ? maxCheckedFiles : Integer.MAX_VALUE; 
		final List<File> files = getFiles(resource, CommonConstants.ROOT_PATH);
		Long returnDate = null;
		for (final File f : files) {
			counter = counter - 1;
			long modTime = f.lastModified();
			returnDate = (returnDate == null || returnDate<modTime) ? modTime : returnDate;
			if (counter <= 0) {
				break;
			}
		}
		return returnDate;
	}
	
	public static XnatResourcecatalog getResource(final XnatImagescandataI scan, final String label)  {
    	if (scan == null) return null;
    	for (final XnatAbstractresourceI resource : scan.getFile()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return (XnatResourcecatalog)resource;
    	}
    	return null;
	}
	
	public static Boolean hasResource(final XnatImagescandataI scan, final String label)  {
    	if (scan == null) return false;
    	for (final XnatAbstractresourceI resource : scan.getFile()) {
    		if (!(resource instanceof XnatResourcecatalog) || resource.getLabel() == null ||
    				!resource.getLabel().equals(label)) {
    			continue;
    		}
    		return true;
    	}
    	return false;
	}
	
	public static List<XnatAbstractresourceI> getResources(final XnatImagesessiondataI session)  {
    	return session.getResources_resource();
	}
	
	public static List<XnatAbstractresourceI> getResources(final XnatImagescandataI scan)  {
    	return scan.getFile();
	}
	
	public static List<XnatResourcecatalog> getResourceCatalogs(final XnatImagescandataI scan)  {
		final List<XnatResourcecatalog> catalogList = new ArrayList<>();
    	for (final XnatAbstractresourceI resource : scan.getFile()) {
    		if (!(resource instanceof XnatResourcecatalog)) {
    			log.debug("DEBUG:  Resource " + resource.getLabel() + " is not a catalog (SERIES_DESCRIPTION=" + scan.getSeriesDescription() + ").");
    			continue;
    		}
    		catalogList.add((XnatResourcecatalog)resource);
    	}
   		return catalogList;
	}
	
	public static XnatResourcecatalog createSessionResource(final AutoXnatExperimentdata thisSession, String resourceLabel, UserI user) throws ServerException, ClientException {
		return createSessionResource(thisSession, resourceLabel, null, null, user);
	}
	
	public static XnatResourcecatalog createSessionResource(final AutoXnatExperimentdata thisSession, String resourceLabel, UserI user, EventMetaI ci) throws ServerException, ClientException {
		return createSessionResource(thisSession, resourceLabel, null, null, user, ci);
	}
	
	public static XnatResourcecatalog createSessionResource(final AutoXnatExperimentdata thisSession, String resourceLabel, String resourceFormat, String resourceContent, UserI user) throws ServerException, ClientException {
		PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, thisSession.getItem(), 
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE));
		} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
			throw new ServerException("ERROR:  Error creating workflow", e);
		}
		EventMetaI ci = wrk.buildEvent();
		return createSessionResource(thisSession, resourceLabel, resourceFormat, resourceContent, user, ci);
	}
	
	public static XnatResourcecatalog createSessionResource(final AutoXnatExperimentdata thisSession, String resourceLabel, String resourceFormat, String resourceContent, UserI user, EventMetaI ci) throws ServerException, ClientException {
		
		if (resourceLabel == null || resourceLabel.length()<1) {
			throw new ClientException("ERROR:  A resource label must be provided");
		}
		
		final XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(thisSession.getProject(), user, false);
		
		final File sessionDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + thisSession.getLabel() +
					File.separator + "RESOURCES" + File.separator + resourceLabel);
		if (!sessionDir.exists()) {
			sessionDir.mkdirs();
		}
		final File catFile = new File(sessionDir,resourceLabel + "_" + thisSession.getId() + CommonConstants.CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ServerException("Couldn't write catalog XML file for " + resourceLabel + " resource",e);
		} catch (Exception e) {
			throw new ServerException("Couldn't write catalog XML file for " + resourceLabel + " resource",e);
		}
		ecat.setLabel(resourceLabel);
		if (resourceFormat != null) {
			ecat.setFormat(resourceFormat);
		}
		if (resourceContent != null) {
			ecat.setContent(resourceContent);
		}
		// Save resource to session
		try {
			thisSession.addResources_resource(ecat);
			if (SaveItemHelper.authorizedSave(thisSession,user,false,true,ci)) {
				//returnList.add(eventStr); 
				return ecat;
			} 
			throw new ServerException("ERROR:  Couldn't create resource " + resourceLabel + " - Unknown reason");
		} catch (Exception e) {
			throw new ServerException("ERROR:  Couldn't add " + resourceLabel + " resource to session - " + e.getMessage(),new Exception());
		}
		
	}
	
	public static XnatResourcecatalog createScanResource(final AutoXnatImagesessiondata thisSession, final AutoXnatImagescandata thisScan, String resourceLabel, UserI user) throws ServerException, ClientException {
		return createScanResource(thisSession, thisScan, resourceLabel, null, null, user);
	}
	
	public static XnatResourcecatalog createScanResource(final AutoXnatImagesessiondata thisSession, final AutoXnatImagescandata thisScan, String resourceLabel, UserI user, EventMetaI ci) throws ServerException, ClientException {
		return createScanResource(thisSession, thisScan, resourceLabel, null, null, user, ci);
	}
	
	public static XnatResourcecatalog createScanResource(final AutoXnatImagesessiondata thisSession, final AutoXnatImagescandata thisScan, String resourceLabel, String resourceFormat, String resourceContent, UserI user) throws ServerException, ClientException {
		PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, thisScan.getItem(), 
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE));
		} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
			throw new ServerException("ERROR:  Error creating workflow", e);
		}
		EventMetaI ci = wrk.buildEvent();
		return createScanResource(thisSession, thisScan, resourceLabel, resourceFormat, resourceContent, user, ci);
	}
	
	public static XnatResourcecatalog createScanResource(final AutoXnatImagesessiondata thisSession, final AutoXnatImagescandata thisScan, String resourceLabel, String resourceFormat, String resourceContent, UserI user, EventMetaI ci) throws ServerException, ClientException {
		
		if (resourceLabel == null || resourceLabel.length()<1) {
			throw new ClientException("ERROR:  A resource label must be provided");
		}
		
		final XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(thisScan.getProject(), user, false);
		
		final File scanDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + proj.getCurrentArc() + File.separator + thisSession.getLabel() +
					File.separator + "SCANS" + File.separator + thisScan.getId() + File.separator + resourceLabel);
		if (!scanDir.exists()) {
			scanDir.mkdirs();
		}
		final File catFile = new File(scanDir,resourceLabel + "_" + thisScan.getId() + CommonConstants.CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ServerException("Couldn't write catalog XML file for " + resourceLabel + " resource",e);
		} catch (Exception e) {
			throw new ServerException("Couldn't write catalog XML file for " + resourceLabel + " resource",e);
		}
		ecat.setLabel(resourceLabel);
		if (resourceFormat != null) {
			ecat.setFormat(resourceFormat);
		}
		if (resourceContent != null) {
			ecat.setContent(resourceContent);
		}
		// Save resource to session
		try {
			thisScan.addFile(ecat);
			if (SaveItemHelper.authorizedSave(thisScan,user,false,true,ci)) {
				//returnList.add(eventStr); 
				return ecat;
			} 
			throw new ServerException("ERROR:  Couldn't create scan resource " + resourceLabel + " - Unknown reason");
		} catch (Exception e) {
			throw new ServerException("ERROR:  Couldn't add " + resourceLabel + " resource to scan - " + e.getMessage(),new Exception());
		}
		
	}
	
	public static XnatResourcecatalog createProjectResource(final AutoXnatProjectdata thisProject, String resourceLabel, UserI user) throws ServerException, ClientException {
		return createProjectResource(thisProject, resourceLabel, null, null, user);
	}
	
	public static XnatResourcecatalog createProjectResource(final AutoXnatProjectdata thisProject, String resourceLabel, UserI user, EventMetaI ci) throws ServerException, ClientException {
		return createProjectResource(thisProject, resourceLabel, null, null, user, ci);
	}
	
	public static XnatResourcecatalog createProjectResource(final AutoXnatProjectdata thisProject, String resourceLabel, String resourceFormat, String resourceContent, UserI user) throws ServerException, ClientException {
		PersistentWorkflowI wrk;
		try {
			wrk = PersistentWorkflowUtils.buildOpenWorkflow(user, thisProject.getItem(), 
						EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, EventUtils.CREATE_RESOURCE));
		} catch (JustificationAbsent | ActionNameAbsent | IDAbsent e) {
			throw new ServerException("ERROR:  Error creating workflow", e);
		}
		EventMetaI ci = wrk.buildEvent();
		return createProjectResource(thisProject, resourceLabel, resourceFormat, resourceContent, user, ci);
	}
	
	public static XnatResourcecatalog createProjectResource(final AutoXnatProjectdata thisProject, String resourceLabel, String resourceFormat, String resourceContent, UserI user, EventMetaI ci) throws ServerException, ClientException {
		
		if (resourceLabel == null || resourceLabel.length()<1) {
			throw new ClientException("ERROR:  A resource label must be provided");
		}
		
		final XnatProjectdata proj = (thisProject instanceof XnatProjectdata) ? (XnatProjectdata) thisProject : 
			XnatProjectdata.getXnatProjectdatasById(thisProject.getId(), user, false);
		
		final File projectDir = new File(ArcSpecManager.GetInstance().getArchivePathForProject(proj.getId()) + 
					File.separator + "resources" + File.separator + resourceLabel);
		if (!projectDir.exists()) {
			projectDir.mkdirs();
		}
		final File catFile = new File(projectDir,resourceLabel + "_" + CommonConstants.CATXML_EXT);
		final CatCatalog cat = new CatCatalog();
		final XnatResourcecatalog ecat = new XnatResourcecatalog();
		try {
			final FileWriter fw = new FileWriter(catFile);
			cat.toXML(fw);
			fw.close();
			// Set URI to archive path
			ecat.setUri(catFile.getAbsolutePath());
		} catch (IOException e) {
			throw new ServerException("Couldn't write catalog XML file for " + resourceLabel + " resource",e);
		} catch (Exception e) {
			throw new ServerException("Couldn't write catalog XML file for " + resourceLabel + " resource",e);
		}
		ecat.setLabel(resourceLabel);
		if (resourceFormat != null) {
			ecat.setFormat(resourceFormat);
		}
		if (resourceContent != null) {
			ecat.setContent(resourceContent);
		}
		// Save resource to session
		try {
			thisProject.addResources_resource(ecat);
			if (SaveItemHelper.authorizedSave(thisProject,user,false,true,ci)) {
				//returnList.add(eventStr); 
				return ecat;
			} 
			throw new ServerException("ERROR:  Couldn't create resource " + resourceLabel + " - Unknown reason");
		} catch (Exception e) {
			throw new ServerException("ERROR:  Couldn't add " + resourceLabel + " resource to session - " + e.getMessage(),new Exception());
		}
		
	}
	
	public static String getResourceSb(XnatResourcecatalog catalog) {
		String project = null;
		String experiment = null;
		String scan = null;
		String resource = null;
		String[] subs = catalog.getUri().split(File.separator);
		for (int i=0; i<subs.length; i++) {
			if (subs[i].equals("arc001")) {
				project=subs[i-1];
				experiment=subs[i+1];
			} else if (subs[i].equals("scans")) {
				scan=subs[i+1];
				resource=subs[i+2];
			} else if (subs[i].equals("RESOURCES")) {
				resource=subs[i+1];
			} else if (subs[i].equals("resources")) {
				project=subs[i-1];
				resource=subs[i+1];
			}
		}
		final StringBuilder sb = new StringBuilder("/archive/projects/");
		sb.append(project);
		if (experiment != null) {
			sb.append("/experiments/").append(experiment);
		}
		if (scan != null) {
			sb.append("/scans/").append(scan);
		}
		if (project == null || resource == null) {
			return null;
		}
		sb.append("/resources/").append(resource);
		return sb.toString();
	}
	
	public static void resourceLocker(XnatResourcecatalog resource, LockOperation operation) {
		final File resourceDirF = ResourceUtils.getResourcePathFile(resource);
		if (!resourceDirF.getPath().contains(File.separator + resource.getLabel())) {
			return;
		}
		for (File f : FileUtils.listFilesAndDirs(resourceDirF, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {
			if (operation.equals(LockOperation.LOCK)) {
				if (f.canWrite()) {
					f.setWritable(false, false);
				}
			} else {
				if (!f.canWrite()) {
					f.setWritable(true, true);
				}
			}
		}
	}


}
