package org.nrg.ccf.common.utilities.components;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.xdat.XDAT;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScriptResult {
	
   	private boolean success; 
   	private String executedCommand; 
   	private String executionNode = ""; 
   	private String status; 
   	private String result; 
	private Integer returnCode; 
	private List<String> stdout;
	private List<String> stderr;
	private Date completedAt;
	
	public ScriptResult() {
	}
	
	public ScriptResult(String executedCommand, boolean success, String status, Integer returnCode, List<String> stdout, List<String> stderr) {
		setExecutedCommand(executedCommand);
		setSuccess(success);
		setStatus(status);
		setReturnCode(returnCode);
		setStdout(stdout);
		setStderr(stderr);
		setCompletedAt(new Date());
		final NodeUtils _nodeUtils = XDAT.getContextService().getBean(NodeUtils.class);
		if (_nodeUtils != null) {
			final String executionNodeStr = _nodeUtils.getXnatNode();
			executionNode = (executionNodeStr!=null) ? executionNodeStr : ""; 
		}
	}
	
	public String buildResultsHtml() {
		final StringBuilder sb = new StringBuilder();
		sb.append("<p><em>Processing completed at: ").append(completedAt).append("</em></p>")
		.append("<p><em>COMMAND: </em>").append(maskPassword(executedCommand)).append("</em></p>")
		.append("<p><em>NODE: </em>").append(executionNode).append("</em></p>")
		.append("<p><em>STATUS: </em>").append(status).append(" (RC=").append(returnCode).append(")</p>")
		.append("<p><em>STDOUT: </em></p><p>").append(stdout).append("</p>")
		.append("<p><em>STDERR: </em></p><p>").append(stderr).append("</p>")
		;
		return sb.toString();
	}

	public String getExecutedCommand() throws ScriptExecException {
		return maskPassword(executedCommand);
	}

	public Long getLongResult() throws ScriptExecException {
		try {
			return Long.valueOf(getStringResult());
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	public Float getFloatResult() throws ScriptExecException {
		try {
			return Float.valueOf(getStringResult());
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	public Integer getIntegerResult() throws ScriptExecException {
		try {
			return Integer.valueOf(getStringResult());
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
	public String getStringResult() throws ScriptExecException {
		if (stdout == null || stdout.isEmpty()) {
			return null;
		} else if (stdout.size()>1) {
			throw new ScriptExecException("ERROR: get[TYPE]Result methods expect a single row if StdOut returned.  The StdOut array contained" +
					stdout.size() + " rows of data");
		} else {
			return stdout.get(0);
		}
	}
	
	public String getMultilineStringResult() {
		if (stdout == null || stdout.isEmpty()) {
			return "";
		} else if (stdout.size()>1) {
			final StringBuilder sb = new StringBuilder();
			final Iterator<String> i = stdout.iterator();
			while (i.hasNext()) {
				sb.append(i.next());
				if (i.hasNext()) {
					sb.append("<br>");
				}
			}
			return sb.toString();
		} else {
			return stdout.get(0);
		}
	}
	
	public List<String> getListResult() throws ScriptExecException {
		if (stdout == null || stdout.isEmpty()) {
			return new ArrayList<>();
		} else {
			return stdout;
		}
	}
	
	@Override
	public String toString() {
		return "ScriptResult [success=" + success + ", status=" + status + ", result=" + result + ", returnCode="
				+ returnCode + ", stdout=" + stdout + ", stderr=" + stderr + "]";
	}
	
	private String maskPassword(String ins) {
		return ins
				.replaceAll("(?i)password *= *[^ ]*", "password=********")
				.replaceAll("(?i)pw *= *[^ ]*", "pw=********")
				.replaceAll("(?i)passwd *= *[^ ]*", "passwd=********")
				.replaceAll("(?i)password *[^ =]", "password ********")
				.replaceAll("(?i)pw *[^ =]", "pw ********")
				.replaceAll("(?i)passwd *[^ =]", "passwd ********")
				;
	}
	
}
