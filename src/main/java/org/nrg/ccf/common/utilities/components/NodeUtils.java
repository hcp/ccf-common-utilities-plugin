package org.nrg.ccf.common.utilities.components;

import org.nrg.framework.node.XnatNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NodeUtils {
	
	private XnatNode _xnatNode;

	@Autowired
	public NodeUtils(XnatNode xnatNode) {
		super();
		_xnatNode = xnatNode;
	}
	
	public String getXnatNode() {
		if (_xnatNode != null && _xnatNode.getNodeId() != null) {
			return _xnatNode.getNodeId();
		}
		return "unknown-node";
	}
	
}
