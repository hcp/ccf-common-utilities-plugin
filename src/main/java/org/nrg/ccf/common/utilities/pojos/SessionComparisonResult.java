package org.nrg.ccf.common.utilities.pojos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SessionComparisonResult {
	
	private Boolean isMatch = false;
	private String result = "";
	
	public static SessionComparisonResult matches() {
		return new SessionComparisonResult(true, "");
	}
	
	public static SessionComparisonResult matches(String status) {
		return new SessionComparisonResult(true, status);
	}
	
	public static SessionComparisonResult doesNotMatch() {
		return new SessionComparisonResult(false, "");
	}
	
	public static SessionComparisonResult doesNotMatch(String status) {
		return new SessionComparisonResult(false, status);
	}

}
