package org.nrg.ccf.common.utilities.nifti;

import org.nrg.xnat.nifti.NiftiHeader;

import ij.io.FileInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NiftiFileInfo {
	
	private FileInfo fileInfo;
	private NiftiHeader niftiHeader;

}
