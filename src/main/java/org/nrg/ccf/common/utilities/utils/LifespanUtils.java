package org.nrg.ccf.common.utilities.utils;

public class LifespanUtils {
	
	public static String getLifespanSiteFromScannerInfo(final String scanner) {
		String site = "";
		if (scanner != null) {
			if (scanner.contains("67056")) {
				site = "Harvard";
			} else if (scanner.contains("35177") || scanner.contains("166038")) {
				site = "WUSTL";
			} else if (scanner.contains("35343") || scanner.contains("35426")) { 
				site = "UCLA";
			} else if (scanner.contains("BAY4OC")) {
				site = "MGH";
			} else if (scanner.contains("TRIOC") || scanner.contains("166007")) {
				site = "UMN";
			}
		}
		return site;
	}


}
