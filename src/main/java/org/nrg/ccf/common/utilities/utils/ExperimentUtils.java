package org.nrg.ccf.common.utilities.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import org.nrg.ccf.common.utilities.pojos.SessionComparisonResult;
import org.nrg.ccf.common.utilities.constants.CommonConstants.ResourceType;
import org.nrg.ccf.common.utilities.pojos.SessionComparisonAdvice;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatExperimentdataFieldI;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xft.search.CriteriaCollection;
import org.nrg.xft.security.UserI;

import com.google.common.collect.ImmutableList;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
public class ExperimentUtils {
	
	// TODO:  Make this part of SessionComparisonAdvice???
	private static List<String> _sourceCheckedQualityValues = ImmutableList.<String>of("unusable");
	private static List<String> _destOverrideQualityValues = ImmutableList.<String>of("undetermined","usable"); 
	
	public static XnatExperimentdataI getExperiment(String projId, String expIdOrLabel, UserI user) {
		
		XnatExperimentdataI exp = XnatExperimentdata.getXnatExperimentdatasById(expIdOrLabel, user, true);
		if (exp == null) { 

	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	    	
	    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
	        subcc1.addClause("xnat:experimentData/project", projId);
	        subcc1.addClause("xnat:experimentData/label", expIdOrLabel);
	        cc.add(subcc1);
	    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
	    	subcc2.addClause("xnat:experimentData/sharing/share/project", projId);
	    	subcc2.addClause("xnat:experimentData/sharing/share/label", expIdOrLabel);
	        cc.add(subcc2);

			final List<XnatExperimentdata> expList = XnatExperimentdata.getXnatExperimentdatasByField(cc, user, true);
			if (!expList.isEmpty()) {
				exp = expList.get(0);
			}
		}
		return exp;
		
	}
	
	public static XnatExperimentdataI getExperimentByLabel(String projId, String expLabel, UserI user) {
		
    	final CriteriaCollection cc=new CriteriaCollection("OR");
    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
        subcc1.addClause("xnat:experimentData/project", projId);
        subcc1.addClause("xnat:experimentData/label", expLabel);
        cc.add(subcc1);
    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
    	subcc2.addClause("xnat:experimentData/sharing/share/project", projId);
    	subcc2.addClause("xnat:experimentData/sharing/share/label", expLabel);
        cc.add(subcc2);

		final List<XnatExperimentdata> expList = XnatExperimentdata.getXnatExperimentdatasByField(cc, user, true);
		return (!expList.isEmpty()) ? expList.get(0) : null;
		
	}
	
	public static XnatMrsessiondata getMrsession(String projId, String expIdOrLabel, UserI user) {
		
		XnatMrsessiondata exp = XnatMrsessiondata.getXnatMrsessiondatasById(expIdOrLabel, user, true);
		if (exp == null) { 

	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	    	
	    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
	        subcc1.addClause("xnat:mrsessionData/project", projId);
	        subcc1.addClause("xnat:mrsessionData/label", expIdOrLabel);
	        cc.add(subcc1);
	    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
	    	subcc2.addClause("xnat:mrsessionData/sharing/share/project", projId);
	    	subcc2.addClause("xnat:mrsessionData/sharing/share/label", expIdOrLabel);
	        cc.add(subcc2);

			final List<XnatMrsessiondata> expList = XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, true);
			if (!expList.isEmpty()) {
				exp = expList.get(0);
			}
		}
		return exp;
		
	}
	
	public static XnatMrsessiondata getMrsessionByLabel(String projId, String expLabel, UserI user) {
		
    	final CriteriaCollection cc=new CriteriaCollection("OR");
    	
    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
        subcc1.addClause("xnat:mrsessionData/project", projId);
        subcc1.addClause("xnat:mrsessionData/label", expLabel);
        cc.add(subcc1);
    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
    	subcc2.addClause("xnat:mrsessionData/sharing/share/project", projId);
    	subcc2.addClause("xnat:mrsessionData/sharing/share/label", expLabel);
        cc.add(subcc2);

		final List<XnatMrsessiondata> expList = XnatMrsessiondata.getXnatMrsessiondatasByField(cc, user, true);
		return (!expList.isEmpty()) ? expList.get(0) : null;
		
	}
	
	public static XnatImagesessiondata getImagesession(String projId, String expIdOrLabel, UserI user) {
		
		XnatImagesessiondata exp = XnatImagesessiondata.getXnatImagesessiondatasById(expIdOrLabel, user, true);
		if (exp == null) { 

	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	    	
	    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
	        subcc1.addClause("xnat:imagesessionData/project", projId);
	        subcc1.addClause("xnat:imagesessionData/label", expIdOrLabel);
	        cc.add(subcc1);
	    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
	    	subcc2.addClause("xnat:imagesessionData/sharing/share/project", projId);
	    	subcc2.addClause("xnat:imagesessionData/sharing/share/label", expIdOrLabel);
	        cc.add(subcc2);

			final List<XnatImagesessiondata> expList = XnatImagesessiondata.getXnatImagesessiondatasByField(cc, user, true);
			if (!expList.isEmpty()) {
				exp = expList.get(0);
			}
		}
		return exp;
		
	}
	
	public static XnatImagesessiondata getImagesessionByLabel(String projId, String expLabel, UserI user) {
		
    	final CriteriaCollection cc=new CriteriaCollection("OR");
    	
    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
        subcc1.addClause("xnat:imagesessionData/project", projId);
        subcc1.addClause("xnat:imagesessionData/label", expLabel);
        cc.add(subcc1);
    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
    	subcc2.addClause("xnat:imagesessionData/sharing/share/project", projId);
    	subcc2.addClause("xnat:imagesessionData/sharing/share/label", expLabel);
        cc.add(subcc2);

		final List<XnatImagesessiondata> expList = XnatImagesessiondata.getXnatImagesessiondatasByField(cc, user, true);
		return (!expList.isEmpty()) ? expList.get(0) : null;
		
	}
	
	// TODO:  Pass SessionComparisonAdvice rather than lists (See above TODO)?
	public static SessionComparisonResult compareSessionQualityAndNotes(XnatImagesessiondataI localExp, 
			XnatImagesessiondataI remoteExp, List<String> sourceCheckedQualityValues, List<String> destOverrideQualityValues) {
		if (!localExp.getXSIType().equals(remoteExp.getXSIType())) {
			return SessionComparisonResult.doesNotMatch("Experiments are different types");
		}
		final List<XnatImagescandataI> localScans = localExp.getScans_scan();
		final List<XnatImagescandataI> remoteScans = remoteExp.getScans_scan();
		if (!(localScans.size() == remoteScans.size())) {
			return SessionComparisonResult.doesNotMatch("Experiments have non-matching scan count (local=" + localScans.size() +", remote=" + remoteScans.size() + ")");
		}
		for (final XnatImagescandataI localScan : localScans) {
			final XnatImagescandataI remoteScan = getCorrespondingScan(remoteScans, localScan);
			final String localQuality = localScan.getQuality();
			final String remoteQuality = remoteScan.getQuality();
			final String localNote = localScan.getNote();
			final String remoteNote = remoteScan.getNote();
			if (sourceCheckedQualityValues.contains(localQuality) && !localQuality.equals(remoteQuality) && destOverrideQualityValues.contains(remoteQuality)) {
				return SessionComparisonResult.doesNotMatch("One or more remote scans don't have local quality value set (localQuality=" + localQuality +
					", remoteQuality=" + remoteQuality + ")");
			}
			if (localNote != null && localNote.trim().length()>0 && !(remoteNote != null && remoteNote.contains(localNote))) {
				return SessionComparisonResult.doesNotMatch("One or more scan notes values is not contained in remote session scan notes (scan=" + localScan.getId() + ")");
			}
		}
		final String localSessionNote = localExp.getNote();
		final String remoteSessionNote = remoteExp.getNote();
		if (localSessionNote != null && localSessionNote.trim().length()>0 && !(remoteSessionNote != null && remoteSessionNote.contains(localSessionNote))) {
			return SessionComparisonResult.doesNotMatch("Local session contains session notes that do not appear in remote session.  Please check.");
		}
		return SessionComparisonResult.matches("Remote session quality and notes values checked okay.");
		
	}
	
	public static boolean isXA30Session(XnatImagesessiondataI exp) {
		
		for (final XnatImagescandataI scan : exp.getScans_scan()) {
			if (ScanUtils.isXa30Scan(scan)) {
				return true;
			}
		}
		return false;
		
	}
	
	public static SessionComparisonResult compareSessions(XnatImagesessiondataI localExp, XnatImagesessiondataI remoteExp, SessionComparisonAdvice advice) {
		
		if (!localExp.getXSIType().equals(remoteExp.getXSIType())) {
			return SessionComparisonResult.doesNotMatch("Experiments are different types");
		}
		final List<XnatImagescandataI> localScans = localExp.getScans_scan();
		final List<XnatImagescandataI> remoteScans = remoteExp.getScans_scan();
		if (!(localScans.size() == remoteScans.size())) {
			return SessionComparisonResult.doesNotMatch("Experiments have non-matching scan count (local=" + localScans.size() +", remote=" + remoteScans.size() + ")");
		}
		for (final XnatImagescandataI localScan : localScans) {
			final XnatImagescandataI remoteScan = getCorrespondingScan(remoteScans, localScan);
			if (remoteScan == null) {
				return SessionComparisonResult.doesNotMatch("Experiments have one or more non-matching scan IDs (local=" + localScan.getId() + ")");
			}
			if (!advice.getAllowDifferingScanTypes() && !localScan.getType().equals(remoteScan.getType())) {
				return SessionComparisonResult.doesNotMatch("Scans have one or more non-matching ScanTypes (local=" + localScan.getType() + 
						", remote=" + remoteScan.getType() + ")");
			}
			if (!advice.getAllowDifferingSeriesDescriptions() && !compareSeriesDescriptions(localScan.getSeriesDescription(),remoteScan.getSeriesDescription())) {
				return SessionComparisonResult.doesNotMatch("Scans have one or more non-matching SeriesDescriptions (local=" + localScan.getSeriesDescription() + 
						", " + remoteScan.getSeriesDescription() + ")");
			}
			final String localQuality = localScan.getQuality();
			final String remoteQuality = remoteScan.getQuality();
			if (!(localQuality == null || remoteQuality == null) && (!localQuality.equals(remoteQuality))) {
				if (remoteQuality.equalsIgnoreCase("UNUSABLE") && !advice.getAllowRemoteSetUnusable()) {
					return SessionComparisonResult.doesNotMatch("One or more remote session scan has been set to unusable");
				} else if (_sourceCheckedQualityValues.contains(localQuality) && !localQuality.equals(remoteQuality) && _destOverrideQualityValues.contains(remoteQuality)) {
					return SessionComparisonResult.doesNotMatch("One or more remote scans don't have local quality value set (localQuality=" + localQuality +
					", remoteQuality=" + remoteQuality + ")");
				}
			}
			final List<XnatAbstractresourceI> localScanResources = ResourceUtils.getResources(localScan);
			final List<XnatAbstractresourceI> remoteScanResources = ResourceUtils.getResources(remoteScan);
			final SessionComparisonResult scanResult = compareResources(localScanResources, remoteScanResources, advice, ResourceType.SCAN,
					"SCAN=" + localScan.getId());
			if (!scanResult.getIsMatch()) {
				return scanResult;
			}
		}
		final List<XnatAbstractresourceI> localSessionResources = ResourceUtils.getResources(localExp);
		final List<XnatAbstractresourceI> remoteSessionResources = ResourceUtils.getResources(remoteExp);
		final SessionComparisonResult sessionResult = compareResources(localSessionResources, remoteSessionResources, advice, ResourceType.SESSION, 
				"SessionLevelResource");
		if (!sessionResult.getIsMatch()) {
			return sessionResult;
		}
		return SessionComparisonResult.matches("Sessions match");
		
	}

	private static boolean compareSeriesDescriptions(String localDesc, String remoteDesc) {
		final String localDescT = (localDesc != null) ? transformDescForComparison(localDesc) : "";
		final String remoteDescT = (remoteDesc != null) ? transformDescForComparison(remoteDesc) : "";
		// Allow us to set series descriptions when null without failing validation.
		return localDescT.equals(remoteDescT) || localDescT.equals(""); 
	}
	
	private static String transformDescForComparison(String desc) {
			// NOTE:  _RR_ may be removed (indicates retro recon) during pre-pipeline processing.
		    if (desc == null) {
		    	return "";
		    }
			return desc.replaceAll("[ +=(]", "_").replaceAll("[&][gl]t;", "_").replaceAll("_RR_", "_")
					.replaceAll("_RR$", "").replaceAll("[<>).:\\[\\]]", "");
	}


	private static SessionComparisonResult compareResources(List<XnatAbstractresourceI> localResources, 
			List<XnatAbstractresourceI> remoteResources, SessionComparisonAdvice advice, ResourceType resourceType, String msgInfo) {
		for (XnatAbstractresourceI localResource : localResources) {
			final String resourceLabel = localResource.getLabel();
			if ( (resourceType.equals(ResourceType.SCAN) && advice.getExcludedScanResources().contains(resourceLabel)) ||
					(resourceType.equals(ResourceType.SESSION) && advice.getExcludedSessionResources().contains(resourceLabel)) ) {
				continue;
			}
			final XnatAbstractresourceI remoteResource = getCorrespondingResource(remoteResources, resourceLabel);
			if (remoteResource == null) {
				return SessionComparisonResult.doesNotMatch("One or more required " + resourceType.toString() + 
						" resources does not exist in the remote session.  Stopped processing at RESOURCE=" + localResource.getLabel() +
						", " + msgInfo);
			}
			final SessionComparisonResult fileResult = compareResourceFiles(localResource, remoteResource, advice, resourceType, msgInfo);
			if (!fileResult.getIsMatch()) {
				return fileResult;
			}
		}
		return SessionComparisonResult.matches();
	}

	private static SessionComparisonResult compareResourceFiles(XnatAbstractresourceI localResource,
			XnatAbstractresourceI remoteResource, SessionComparisonAdvice advice, ResourceType resourceType, String msgInfo) {
		final List<File> localFiles = ResourceUtils.getFiles(localResource);
		final String localResourcePath = ResourceUtils.getResourcePath(localResource);
		final List<File> remoteFiles = ResourceUtils.getFiles(remoteResource);
		final String remoteResourcePath = ResourceUtils.getResourcePath(remoteResource);
		if ( (resourceType.equals(ResourceType.SCAN) && advice.getFileCountOnlyScanResources().contains(localResource.getLabel())) ||
				(resourceType.equals(ResourceType.SESSION) && advice.getFileCountOnlySessionResources().contains(localResource.getLabel()))
				) {
			if (!(localFiles.size() == remoteFiles.size())) {
				return SessionComparisonResult.doesNotMatch("Resource file count doesn't match.  Stopped processing at (RESOURCE=" +
						localResource.getLabel() + "," + msgInfo + ")");
			}
			return SessionComparisonResult.matches();
		}
		for (final File localFile : localFiles) {
			final SessionComparisonResult fileResult = hasCorrespondingNewerFile(localFile, localResourcePath, remoteFiles, remoteResourcePath, advice, msgInfo);
			if (!fileResult.getIsMatch()) {
				return fileResult;
			}
		}
		if (!advice.getAllowAddedRemoteFiles()) {
			for (final File remoteFile : remoteFiles) {
				final SessionComparisonResult fileResult = hasCorrespondingFile(remoteFile, remoteResourcePath, localFiles, localResourcePath, advice, msgInfo);
				if (!fileResult.getIsMatch()) {
					return fileResult;
				}
			}
		}
		return SessionComparisonResult.matches();
	}

	private static SessionComparisonResult hasCorrespondingNewerFile(File localFile, String localResourcePath, List<File> remoteFiles,
			String remoteResourcePath, SessionComparisonAdvice advice, String msgInfo) {
		final String localPathCompare = localFile.getPath().replaceFirst(localResourcePath, "");
		for (final File remoteFile : remoteFiles) {
			final String remotePathCompare = remoteFile.getPath().replaceFirst(remoteResourcePath, "");
			if (localPathCompare.equals(remotePathCompare) || alternatePathMatches(localPathCompare, remotePathCompare, advice.getAlternateFilePathTransforms())) {
				if (remoteFile.lastModified()>=localFile.lastModified()) {
					return SessionComparisonResult.matches();
				} else {
					try {
						if (!compareMd5SumsEqual(localFile,remoteFile)) {
							return SessionComparisonResult.doesNotMatch(
									"Source session file is newer than destination file and checksums do not match (" +
									msgInfo + ", FILE=" + localFile.getPath() + ")");
						} else {
							return SessionComparisonResult.matches();
							
						}
					} catch (IOException e) {
						return SessionComparisonResult.doesNotMatch(
								"Source session file is newer than destination file and checksums could not be compared (" +
								msgInfo + ", FILE=" + localFile.getPath() + ")");
					}
				}
			} 
		}
		return SessionComparisonResult.doesNotMatch("No matching file in destination session (FILE=" + localFile.getPath() + ")");
	}

	private static boolean compareMd5SumsEqual(File localFile, File remoteFile) throws IOException {
		if (localFile == null && remoteFile == null) {
			return true;
		}
		if (localFile == null || remoteFile == null) {
			return false;
		}
		return DigestUtils.md5Hex(FileUtils.openInputStream(localFile)).equals(
				DigestUtils.md5Hex(FileUtils.openInputStream(remoteFile)));
	}

	private static boolean alternatePathMatches(String localPathCompare, String remotePathCompare,
			Map<String, Map<String, String>> alternateFilePathTransforms) {
		for (final String pathMatch : alternateFilePathTransforms.keySet()) {
			if (localPathCompare.matches(pathMatch)) {
				for (final Entry<String, String> transMap : alternateFilePathTransforms.get(pathMatch).entrySet()) {
					if ( localPathCompare.replaceFirst(transMap.getKey(), transMap.getValue()).equals(remotePathCompare) ||
						localPathCompare.replaceAll(transMap.getKey(), transMap.getValue()).equals(remotePathCompare)) {
						return true;
					}
				}
			}
			
		}
		return false;
	}

	private static SessionComparisonResult hasCorrespondingFile(File remoteFile, String remoteResourcePath, List<File> localFiles,
			String localResourcePath, SessionComparisonAdvice advice, String msgInfo) {
		final String remotePathCompare = remoteFile.getPath().replaceFirst(remoteResourcePath, "");
		for (final File localFile : localFiles) {
			final String localPathCompare = localFile.getPath().replaceFirst(localResourcePath, "");
			if (localPathCompare.equals(remotePathCompare) || alternatePathMatches(localPathCompare, remotePathCompare, advice.getAlternateFilePathTransforms())) {
				return SessionComparisonResult.matches();
			}
		}
		return SessionComparisonResult.doesNotMatch("No matching destination file in source session (" + msgInfo + ", FILE=" + remoteFile.getPath() + ")");
	}

	public static XnatImagescandataI getCorrespondingScan(List<XnatImagescandataI> remoteScans, XnatImagescandataI localScan) {
		if (remoteScans == null || localScan == null) {
			return null;
		}
		final String scanId = localScan.getId();
		for (final XnatImagescandataI remoteScan : remoteScans) {
			if (scanId != null && remoteScan.getId() != null && scanId.equals(remoteScan.getId())) {
				return remoteScan;
			}
		}
		return null;
	}

	public static XnatAbstractresourceI getCorrespondingResource(List<XnatAbstractresourceI> resources, String resourceLabel) {
		if (resources == null) {
			return null;
		}
		for (final XnatAbstractresourceI resource : resources) {
			// Handle resources without labels
			if (resource != null && (resource.getLabel() == null && resourceLabel == null) || 
					(resource.getLabel() != null && resourceLabel != null && resource.getLabel().equals(resourceLabel))) {
				return resource;
			}
		}
		return null;
	}

	public static String getFieldValue(XnatImagesessiondataI exp, String fieldName) {
		if (exp == null || fieldName == null) {
			return null;
		}
		for (final XnatExperimentdataFieldI field : exp.getFields_field()) {
			if (field != null && field.getName() != null && field.getName().equals(fieldName)) {
				return field.getField();
			}
		}
		return null;
	}

}
