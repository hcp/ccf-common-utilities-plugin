package org.nrg.ccf.common.utilities.components;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

//@Slf4j
@Component
public class XnatUtils {
	
	private JdbcTemplate _jdbcTemplate;

	@Autowired
	public XnatUtils(JdbcTemplate jdbcTemplate) {
		super();
		_jdbcTemplate = jdbcTemplate;
	}
	
	public List<String> getImageSessionSeriesDescriptions(String projectID) {
		return _jdbcTemplate.queryForList(
				"SELECT distinct  SD.series_description " +
				  "FROM xnat_experimentdata ED INNER JOIN xnat_imagescandata SD " +
				    "ON ED.id = SD.image_session_id and ED.project = ? ORDER BY series_description", 
			   new Object[] { projectID }, String.class);
	}

}
