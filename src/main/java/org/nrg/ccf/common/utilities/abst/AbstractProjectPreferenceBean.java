package org.nrg.ccf.common.utilities.abst;

import javax.annotation.PostConstruct;

import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.framework.constants.Scope;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.services.NrgPreferenceService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractProjectPreferenceBean extends AbstractPreferenceBean {
	
	private static final long serialVersionUID = -8460735760853454005L;
	protected PreferenceUtils _preferenceUtils;
	public static final Scope SCOPE = Scope.Project;
	
	// TODO:  This is a temporary workaround for an XNAT bug (XNAT-5134).  When 
	// time permits, that bug should be addressed.
	@PostConstruct
	public void initIt() {
		final NrgPreferenceBean preferenceBean = this.getClass().getAnnotation(NrgPreferenceBean.class);
		final String toolId = preferenceBean.toolId();
		final int removed = _preferenceUtils.removeSiteLevelPreferencesForTool(toolId);
		if (removed>0) {
			log.warn("WARNING:  Removed " + removed + 
					" incorrect site-level preference rows for project preference class " +
					this.getClass().getName());
		}
	}

	protected AbstractProjectPreferenceBean(NrgPreferenceService preferenceService, PreferenceUtils preferenceUtils) {
		super(preferenceService);
		_preferenceUtils = preferenceUtils;
	}

}
