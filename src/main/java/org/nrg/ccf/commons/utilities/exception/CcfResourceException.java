package org.nrg.ccf.commons.utilities.exception;

public class CcfResourceException extends Exception {
	
	private static final long serialVersionUID = -3606239266366341900L;

	public CcfResourceException() {
		super();
	}

	public CcfResourceException(String message) {
		super(message);
	}

	public CcfResourceException(Throwable cause) {
		super(cause);
	}

	public CcfResourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public CcfResourceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
