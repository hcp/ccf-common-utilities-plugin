package org.nrg.ccf.commons.utilities.exception;

public class CcfProjectNotSupportedException extends Exception {
	
	private static final long serialVersionUID = -3606239266366341900L;

	public CcfProjectNotSupportedException() {
		super();
	}

	public CcfProjectNotSupportedException(String message) {
		super(message);
	}

	public CcfProjectNotSupportedException(Throwable cause) {
		super(cause);
	}

	public CcfProjectNotSupportedException(String message, Throwable cause) {
		super(message, cause);
	}

	public CcfProjectNotSupportedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
