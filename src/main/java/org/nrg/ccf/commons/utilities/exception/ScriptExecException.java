package org.nrg.ccf.commons.utilities.exception;

public class ScriptExecException extends Exception {
	
	private static final long serialVersionUID = -5226943680165842594L;

	public ScriptExecException() {
		super();
	}

	public ScriptExecException(String message) {
		super(message);
	}

	public ScriptExecException(Throwable cause) {
		super(cause);
	}

	public ScriptExecException(String message, Throwable cause) {
		super(message, cause);
	}

	public ScriptExecException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
